//
//  GradientView.h
//  PanColorPicker
//
//  Created by LivingPlace on 27.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradientView : UIView

// this property is used to set the filling size of the 
// bounds this View is drawn in. 
// 0.9 means the gradients radius is filling 90% of the view
@property (nonatomic) float scale;
@property (nonatomic, weak) UIColor *choosenColor;
@property (nonatomic, strong) UIImage *image;

-(void)pan:(UIPanGestureRecognizer *)gesture;

@end
