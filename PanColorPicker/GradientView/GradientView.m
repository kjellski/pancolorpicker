//
//  GradientView.m
//  PanColorPicker
//
//  Created by LivingPlace on 27.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
// from http://en.wikipedia.org/wiki/Radian
#define DEGREES_PER_RADIAN 57.29577f

// one for each color code in hue color space
#define COLOR_COUNT 256.0f


#import "GradientView.h"
#import <math.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface GradientView()
@property (readonly) CGPoint centerPoint;
@end

@implementation GradientView

@synthesize image = _image;
@synthesize scale = _scale;
@synthesize centerPoint = _centerPoint;
@synthesize choosenColor = _choosenColor;

-(UIColor *)getUIColorAtCGPoint:(CGPoint) point
{
    CGImageRef cgim = self.image.CGImage;
    unsigned char pixel[4] = {0};
    size_t bitsPerComponent = CGImageGetBitsPerComponent(cgim);
    size_t bytesPerRow = CGImageGetBytesPerRow(cgim); 
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(cgim);
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(cgim);
    CGContextRef context = CGBitmapContextCreate(pixel,
                                                 1, 
                                                 1, 
                                                 bitsPerComponent, 
                                                 bytesPerRow, 
                                                 colorSpace,
                                                 alphaInfo);
    CGContextDrawImage(context, CGRectMake(-point.x, 
                                           -point.y, 
                                           CGImageGetWidth(cgim), 
                                           CGImageGetHeight(cgim)), 
                       cgim);
    CGContextRelease(context);
    UIColor *result = [UIColor colorWithRed:(pixel[1]/255.0f) 
                                      green:(pixel[3]/255.0f) 
                                       blue:(pixel[2]/255.0f) 
                                      alpha:(pixel[0]/255.0f)];
    return result;
}

-(void)pan:(UIPanGestureRecognizer *)gesture
{
    if((gesture.state == UIGestureRecognizerStateChanged) ||
       (gesture.state == UIGestureRecognizerStateEnded)) 
    {
        CGPoint touchPoint = [gesture locationInView:gesture.view];
        UIColor *touchColor = [self getUIColorAtCGPoint:touchPoint];
        self.backgroundColor = touchColor;
        //NSLog(@"P:%@ C:%@", [NSValue valueWithCGPoint:touchPoint], touchColor);
    }
}

-(UIImage*) makeImage {
    
    UIGraphicsBeginImageContext(self.bounds.size);
    
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return viewImage;
}

-(UIColor *)choosenColor
{
    if(!_choosenColor)
        return [UIColor whiteColor];
    else
        return _choosenColor;
}

-(UIImage *)image  
{
    if (_image.size.width == 0 &&
        _image.size.height == 0) {
        _image = [self makeImage];
    }
    return _image;
}

-(void)setImage:(UIImage *)image
{
    if (_image != image)
    {
        _image = image;
    }
}

-(float)scale
{
    if(!_scale){
        return 0.9f;
    }
    else 
        return _scale;
}

// only redraw if needed
-(void)setScale:(float)scale
{
    if (_scale != scale)
    {
        _scale = scale;
        [self setNeedsDisplay];
    }
}

-(CGPoint)centerPoint
{
    if(_centerPoint.x == 0 || _centerPoint.y == 0){
        CGPoint center;
        center.x = self.bounds.origin.x + self.bounds.size.width / 2;
        center.y = self.bounds.origin.y + self.bounds.size.height / 2;
        return center;
    }
    else 
        return _centerPoint;
}

-(int) getRadiusInPercent:(float) percent
{
    // size of the rect depending on the smalle side
    int radius = (int)((self.bounds.size.width / 2) * percent);
    if (self.bounds.size.height < self.bounds.size.width) {
        radius = (int)((self.bounds.size.height / 2) *percent);
    }
    return radius;
}

-(void) setup
{
    self.contentMode = UIViewContentModeRedraw;
}

-(void) awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(CGPoint) getTargetPointFromCenterPoint:(CGPoint)center
                               atDegrees:(double)degrees
                              withRadius:(double)radius
{
    double radians = (double)(degrees / DEGREES_PER_RADIAN);
    
    CGPoint resultingPoint;
    
    double cfloored = floor(radius * cos(radians));
    double sfloored = floor(radius * sin(radians));
    
    resultingPoint.x = center.x + cfloored;
    resultingPoint.y = center.y + sfloored;
//    NSLog(@"P:%@", [NSValue valueWithCGPoint:resultingPoint]);
    return resultingPoint;
}

-(NSArray *) getAllPointsFromCenterPoint:(CGPoint)center
                              withRadius:(double)radius
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:(COLOR_COUNT - 1)];
    for (int i = 0; i < COLOR_COUNT - 1; i++) {
        double degrees = (double)((i * 361.0f) / (COLOR_COUNT - 1));
        
        CGPoint pointToAdd = [self getTargetPointFromCenterPoint:center 
                                                       atDegrees:degrees
                                                      withRadius:radius]; 
        [result insertObject:[NSValue valueWithCGPoint:pointToAdd] atIndex:i];
    }
    
    return [result copy];
}

-(NSArray *) getColors
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:(COLOR_COUNT - 1)];
    
    for (int i = 0; i < COLOR_COUNT - 1; i++) 
    {
        double hue = (i * 1.0f/ COLOR_COUNT);
        UIColor *color = [UIColor colorWithHue:hue
                                    saturation:1.0
                                    brightness:1.0
                                         alpha:1.0];// CGColor];
//        NSLog(@"C(%d):%@", i, color);
        [result insertObject:color atIndex:i];
    }
    
    return [result copy];
}

-(void) drawGradient:(CGPoint)targetPoint
           withColor:(UIColor *)color
           inContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    UIColor *startColor = [UIColor whiteColor];
    CGFloat *startColorComponents =
    (CGFloat *)CGColorGetComponents([startColor CGColor]);
    
    UIColor *endColor = color;
    CGFloat *endColorComponents =
    (CGFloat *)CGColorGetComponents([endColor CGColor]);
    
    CGFloat colorComponents[8] = {	
        /* Four components of the orange color (RGBA) */
        startColorComponents[0],
        startColorComponents[1],
        startColorComponents[2],
        startColorComponents[3], /* First color = orange */
        
        
        /* Four components of the blue color (RGBA) */
        endColorComponents[0],
        endColorComponents[1],
        endColorComponents[2],
        endColorComponents[3], /* Second color = blue */
    };
    
    size_t num_locations = 2;
	CGFloat locations[3] = { 0.0, 1.0 };
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents
    (colorSpace,
     (const CGFloat *)&colorComponents,
     locations,
     num_locations);
    
    CGColorSpaceRelease(colorSpace);
    
    
    CGPoint endCenter = targetPoint;     
    CGFloat startRadius, endRadius;
    startRadius = 1; 
    endRadius = 3;
    
    CGContextDrawRadialGradient(context, gradient, self.centerPoint, startRadius,endCenter, endRadius, kCGGradientDrawsBeforeStartLocation);
    
    
    CGGradientRelease(gradient);
    
    
    UIGraphicsPopContext();
}

-(void)drawCircleArrountGradientinContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    
    CGContextBeginPath(context);
    CGContextSetLineWidth(context, 3);
    CGContextAddArc(context, self.centerPoint.x, self.centerPoint.y, [self getRadiusInPercent:self.scale] + 4, 0, 2*M_PI, YES);
    CGContextStrokePath(context);
    
    UIGraphicsPopContext();
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    // get the points and color for this gradients circle
    NSArray *points = [self getAllPointsFromCenterPoint:self.centerPoint
                                             withRadius:[self getRadiusInPercent:self.scale]];
    NSArray *colors = [self getColors];
    
    // for each dot in the circle, draw one gradient from the center to that point
    for (int i = 0; i < [points count] - 1; i++ ) {
        CGPoint targetPoint = [[points objectAtIndex:i] CGPointValue];
        [self drawGradient:targetPoint
                 withColor:[colors objectAtIndex:i] 
                 inContext:context];
    }
    
    [self drawCircleArrountGradientinContext:context];
    CGContextRestoreGState(context);
}

@end

