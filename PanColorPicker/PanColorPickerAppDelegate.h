//
//  PanColorPickerAppDelegate.h
//  PanColorPicker
//
//  Created by LivingPlace on 20.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PanColorPickerAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
