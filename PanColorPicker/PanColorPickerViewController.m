//
//  PanColorPickerViewController.m
//  PanColorPicker
//
//  Created by LivingPlace on 20.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "PanColorPickerViewController.h"

@interface  PanColorPickerViewController()
@property (nonatomic, weak) IBOutlet GradientView *gradientView;
@end

@implementation PanColorPickerViewController

@synthesize gradientView = _gradientView;


-(void)setGradientView:(GradientView *)gradientView
{
    _gradientView = gradientView;
    [self.gradientView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self.gradientView action:@selector(pan:)]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.gradientView];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
