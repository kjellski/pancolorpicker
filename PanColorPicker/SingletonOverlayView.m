//
//  SingletonOverlayView.m
//  PanColorPicker
//
//  Created by LivingPlace on 20.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SingletonOverlayView.h"


@implementation SingletonOverlayView

+ (SingletonOverlayView *)sharedSingleton
{
    static SingletonOverlayView *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[SingletonOverlayView alloc] init];
        
        return sharedSingleton;
    }
}

- (id)initWithFrame:(CGRect)frame
{
    return nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
