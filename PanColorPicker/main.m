//
//  main.m
//  PanColorPicker
//
//  Created by LivingPlace on 20.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PanColorPickerAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PanColorPickerAppDelegate class]));
    }
}
